//create a standard server app.

//get the http module using the require() directive. repackage the module on a new variable

const http = require('http');


const host = 4000;
//create the server and place it inside a new variable to give it an identifier
//arguments => They are used to 'Catch' data and pass it along inside our function.
let server = http.createServer( (req, res) => {
	res.end(`Welcome to the App`);
})

//Assign a designated port that will serve the project by binding the connection with the desired port.

	server.listen(host);

//include a response that will display in the console to varify the connection that was establish.

	console.log(`listen on port: ${host}`);
